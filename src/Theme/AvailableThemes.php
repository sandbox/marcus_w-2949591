<?php

namespace Drupal\commerce_store_theme\Theme;

/**
 * Class AvailableThemes.
 *
 * Shows the possible themes.
 */
class AvailableThemes {

  /**
   * Gets the allowed values for the 'theme' base field of the store.
   *
   * @return array
   *   The allowed values.
   */
  public static function getList() {
    /** @var \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler */
    $theme_handler = \Drupal::service('theme_handler');
    $themes = $theme_handler->rebuildThemeData();
    $options = [];

    foreach ($themes as &$theme) {
      if (!empty($theme->info['hidden'])) {
        continue;
      }

      if (!empty($theme->status) && $theme->status === 1) {
        $options[$theme->getName()] = $theme->info['name'];
      }
    }
    return $options;
  }

}
