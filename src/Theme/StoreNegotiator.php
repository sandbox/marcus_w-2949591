<?php

namespace Drupal\commerce_store_theme\Theme;

use Drupal\commerce_store\CurrentStore;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Class StoreNegotiator.
 *
 * Finds the theme for a store.
 */
class StoreNegotiator implements ThemeNegotiatorInterface {

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStore
   */
  protected $store;

  /**
   * Creates a new StoreNegotiator instance.
   *
   * @param \Drupal\commerce_store\CurrentStore $store
   *   The current store.
   */
  public function __construct(CurrentStore $store) {
    $this->store = $store;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $store = $this->store->getStore();
    if ($store !== NULL) {
      $theme = $store->get('theme')->getValue();
      return !empty($theme) ? $theme[0]['value'] : NULL;
    }
    return NULL;
  }

}
